<div class="container">
	<div class="row">
		<div class="eight columns">
			<?php if ($logo): ?>
		        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
          			<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
		        </a>
      		<?php endif; ?>
      		
      		<?php if ($site_name || $site_slogan): ?>
        <div id="name-and-slogan">
          <?php if ($site_name): ?>
            <?php if ($title): ?>
              <div id="site-name"><strong>
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
              </strong></div>
            <?php else: /* Use h1 when the content title is empty */ ?>
              <h1 id="site-name">
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
              </h1>
            <?php endif; ?>
          <?php endif; ?>

          <?php if ($site_slogan): ?>
            <div id="site-slogan"><?php print $site_slogan; ?></div>
          <?php endif; ?>
        </div> <!-- /#name-and-slogan -->
      <?php endif; ?>
		</div>
		<div class="four columns">
		        <?php print theme('links__system_secondary_menu', array('links' => $secondary_menu, 'attributes' => array('id' => 'secondary-menu', 'class' => array('nav-bar')),)); ?>
	
		</div>
	</div>
	
	<div class="row">
		<div class="twelve columns">
			<?php print render($page['header']); ?>
		</div>
	</div>

	<div class="row">
		<div class="twelve columns">
	    <?php if ($main_menu || $secondary_menu): ?>
      <div id="navigation"><div class="section">
        <?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('id' => 'main-menu', 'class' => array('nav-bar')),)); ?>
      </div></div> <!-- /.section, /#navigation -->
    <?php endif; ?>
    		</div>
	</div>
		
	<?php if ($page['highlighted']): ?>
	<div class="row">
		<div class="twelve columns">
			<div id="highlighted"><?php print render($page['highlighted']); ?></div>
		</div>
	</div>
	<?php endif; ?>
	
	<div class="row">

	
	<?php if ($page['sidebar_first']): ?>
		<div class="three columns">
        <div id="sidebar-first"><div class="section">
          <?php print render($page['sidebar_first']); ?>
        </div></div> <!-- /.section, /#sidebar-first -->
        </div>
    <?php endif; ?>
    
	<div id="content" class="<?php print fast_foundation_content_columns(&$page); ?> columns">
    
   <div class="section">
        
        <a id="main-content"></a>
        <?php print render($title_prefix); ?>
        <?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
        <?php print render($title_suffix); ?>
        <?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
        <?php print render($page['help']); ?>
        <?php print $messages; ?>
        <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
        <?php print render($page['content']); ?>
        <?php // print $feed_icons; ?>
      
      	<?php if ($breadcrumb): ?>
      <div id="breadcrumb"><?php print $breadcrumb; ?></div>
    <?php endif; ?>
     </div> <!-- /.section, /#content -->
	

    
    </div>
    
    	<?php if ($page['sidebar_second']): ?>
		<div class="three columns">
        <div id="sidebar-first"><div class="section">
          <?php print render($page['sidebar_second']); ?>
        </div></div> <!-- /.section, /#sidebar-first -->
        </div>
    <?php endif; ?>
    
    </div> <!-- /.row close -->
    
    
    
	<div class="row">	
		<div class="twelve columns">
			<hr/>
			<?php print render($page['footer']); ?>
		</div>
	</div>
	
</div> <!-- /.container close -->