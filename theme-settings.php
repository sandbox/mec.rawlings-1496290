<?php

/* Theme settings */

function fast_foundation_form_system_theme_settings_alter(&$form, &$form_state) {
	$form['zurbfoundation_settings'] = array(
		'#type' => 'fieldset',
		'#title' => t('Theme Options'),
	);
	$form['zurbfoundation_settings']['nice_tabs'] = array(
		'#type'          => 'checkbox',
		'#title'         => t('Use nice tabs'),
		'#default_value' => theme_get_setting('nice_tabs'),
		'#description'   => t("Turn nice tabs on or off."),
	);
	$form['zurbfoundation_settings']['nice_buttons'] = array(
		'#type'          => 'checkbox',
		'#title'         => t('Use nice buttons'),
		'#default_value' => theme_get_setting('nice_buttons'),
		'#description'   => t("Turn nice buttons on or off."),
	);
	
}

