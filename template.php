<?php

/**
 * adding Foundation button classes
 */
function fast_foundation_button($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'submit';
  element_set_attributes($element, array('id', 'name', 'value'));
  
  // get our theme settings and set the right class values
   	$button_class = '';
   	if ( theme_get_setting('nice_buttons') == 1 ) { $button_class = 'nice '; }

	// if the button is disabled we also add the Foundation class
  $element['#attributes']['class'][] = $button_class.' radius button form-' . $element['#button_type'];
  
  if (!empty($element['#attributes']['disabled'])) {
    $element['#attributes']['class'][] = 'form-button-disabled '.$button_class.' .btn-disabled ';
  }
  
  // if the value is save, add the Foundation class
  if ($element['#attributes']['value'] == "Save") {
  	$element['#attributes']['class'][] = $button_class.'blue btn-primary';
  } else if ($element['#attributes']['value'] == "Search") {
  	$element['#attributes']['class'][] = $button_class.'blue btn-primary';
  } else {
  	$element['#attributes']['class'][] = $button_class.'white';
  }

  return '<input' . drupal_attributes($element['#attributes']) . ' />';
}

/**
 * adding Foundation tab classes and changing markup to <dd> and <dl>
 */
function fast_foundation_menu_local_tasks(&$variables) {
	//print_r($variables['primary']);
  $output = '';

  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<h3 class="element-invisible">' . t('Primary tabs') . '</h3>';
   	// get our theme settings and set the right class values
   	$tab_class = '';
   	if ( theme_get_setting('nice_tabs') == 1 ) { $tab_class = 'nice'; }
   
    $variables['primary']['#prefix'] .= '<dl class="'.$tab_class.' tabs">';
    
    $variables['primary']['#suffix'] = '</dl>';
    $output .= drupal_render($variables['primary']);
  }
  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<h3 class="element-invisible">' . t('Secondary tabs') . '</h3>';
    $variables['secondary']['#prefix'] .= '<dl class="sub-nav">';
    $variables['secondary']['#suffix'] = '</dl>';
    $output .= drupal_render($variables['secondary']);
  }

  return $output;
}

/**
 * adding Foundation menu classes and changing markup to <dd> and <dl>
 */
function fast_foundation_menu_local_task($variables) {
  $link = $variables['element']['#link'];
  $link_text = $link['title'];

  if (!empty($variables['element']['#active'])) {
    // Add text to indicate active tab for non-visual users.
    $active = '<span class="element-invisible">' . t('(active tab)') . '</span>';

    // If the link does not contain HTML already, check_plain() it now.
    // After we set 'html'=TRUE the link will not be sanitized by l().
    if (empty($link['localized_options']['html'])) {
      $link['title'] = check_plain($link['title']);
    }
    $link['localized_options']['html'] = TRUE;
    $link_text = t('!local-task-title!active', array('!local-task-title' => $link['title'], '!active' => $active));
  }

  return '<dd' . (!empty($variables['element']['#active']) ? ' class="active"' : '') . '>' . l($link_text, $link['href'], $link['localized_options']) . "</dd>\n";
}


/**
 * adding Foundation status message classes
 */
function fast_foundation_status_messages($variables) {
  $display = $variables['display'];
  $output = '';

  $status_heading = array(
    'status' => t('Status message'), 
    'error' => t('Error message'), 
    'warning' => t('Warning message'),
  );
  
  foreach (drupal_get_messages($display) as $type => $messages) {
  	
  	// we've added the Bootstrap message types
  	switch($type) {
  		case "status":
  			$output .= "<div class=\"alert-box success\">\n";
  			break;
  		case "error":
  			$output .= "<div class=\"alert-box error\">\n";
  			break;
  		case "warning":
  			$output .= "<div class=\"alert-box\">\n";
  			break;
  		default:
  			$output .= "<div class=\"alert-box\">\n";
  	};  
  	// adding the Bootstrap close button
  	$output .= "<a class=\"close\" data-dismiss=\"alert\">&times;</a>\n";
    
    if (!empty($status_heading[$type])) {
      $output .= '<h2 class="element-invisible">' . $status_heading[$type] . "</h2>\n";
    }
    if (count($messages) > 1) {
      $output .= " <ul>\n";
      foreach ($messages as $message) {
        $output .= '  <li>' . $message . "</li>\n";
      }
      $output .= " </ul>\n";
    }
    else {
      $output .= $messages[0];
    }
    $output .= "</div>\n";
  }
  return $output;
}

/**
 * adding breadcrumb classes and seperator
 */
function fast_foundation_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
	
	
	
  if (!empty($breadcrumb)) {
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';
	
	// we output the breadcrumbs as a ul to match Bootstrap
	$output .= "<ul class=\"breadcrumbs\">\n";
	foreach($breadcrumb as $crumb) {
		$output .= "<li>". $crumb ."<span class=\"divider\"></span></li>\n";
	}
	// we want the page title on the end of the bread
	$page_title = drupal_get_title();
	$output .= "<li class=\"current\"><span>". $page_title ."</span></li>\n";
	
	
	$output .= "</ul>\n";
    return $output;
  }
}

/**
 * pagination markup
 */
function fast_foundation_pager($variables) {
  $tags = $variables['tags'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $quantity = $variables['quantity'];
  global $pager_page_array, $pager_total;

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.

  $li_first = theme('pager_first', array('text' => (isset($tags[0]) ? $tags[0] : t('first')), 'element' => $element, 'parameters' => $parameters));
  $li_previous = theme('pager_previous', array('text' => (isset($tags[1]) ? $tags[1] : t('previous')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_next = theme('pager_next', array('text' => (isset($tags[3]) ? $tags[3] : t('next')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_last = theme('pager_last', array('text' => (isset($tags[4]) ? $tags[4] : 'last'), 'element' => $element, 'parameters' => $parameters));

  if ($pager_total[$element] > 1) {
    if ($li_first) {
      $items[] = array(
        'class' => array('pager-first'), 
        'data' => $li_first,
      );
    }
    if ($li_previous) {
      $items[] = array(
        'class' => array('pager-previous'), 
        'data' => $li_previous,
      );
    }

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
      if ($i > 1) {
        $items[] = array(
          'class' => array('pager-ellipsis'), 
          'data' => '�',
        );
      }
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $items[] = array(
            'class' => array('pager-item'), 
            'data' => theme('pager_previous', array('text' => $i, 'element' => $element, 'interval' => ($pager_current - $i), 'parameters' => $parameters)),
          );
        }
        if ($i == $pager_current) {
          $items[] = array(
            'class' => array('current'), 
            'data' => '<a href="">'. $i. '</a>',
          );
        }
        if ($i > $pager_current) {
          $items[] = array(
            'class' => array('pager-item'), 
            'data' => theme('pager_next', array('text' => $i, 'element' => $element, 'interval' => ($i - $pager_current), 'parameters' => $parameters)),
          );
        }
      }
      if ($i < $pager_max) {
        $items[] = array(
          'class' => array('pager-ellipsis'), 
          'data' => '�',
        );
      }
    }
    // End generation.
    if ($li_next) {
      $items[] = array(
        'class' => array('pager-next'), 
        'data' => $li_next,
      );
    }
    if ($li_last) {
      $items[] = array(
        'class' => array('pager-last'), 
        'data' => $li_last,
      );
    }
    return '<h2 class="element-invisible">' . t('Pages') . '</h2>' . theme('item_list', array(
      'items' => $items, 
      'attributes' => array('class' => array('pagination')),
    ));
  }
}

/**
 * modify forms
 */
function fast_foundation_form_alter(&$form, &$form_state, $form_id) {
	// search form
	if ($form_id == 'search_block_form') {
   		// foundation specific class
	    $form['search_block_form']['#attributes']= array('class' => array('expand input-text'), 'placeholder' => 'Search');    
    	// clear the button classes and then add the blue back in
    	//print_r($form);
    	}
  	
  	// node delete confimation edit form
   	if ($form_id == 'node-delete-confirm') {
   
  	}
} 

/**
 * menu trees
 */
function fast_foundation_menu_tree_output($tree) {
  $build = array();
  $items = array();

  // Pull out just the menu links we are going to render so that we
  // get an accurate count for the first/last classes.
  foreach ($tree as $data) {
    if ($data['link']['access'] && !$data['link']['hidden']) {
      $items[] = $data;
    }
  }

  $router_item = menu_get_item();
  $num_items = count($items);
  foreach ($items as $i => $data) {
    $class = array();
    if ($i == 0) {
      $class[] = 'first';
    }
    if ($i == $num_items - 1) {
      $class[] = 'last';
    }
    // Set a class for the <li>-tag. Since $data['below'] may contain local
    // tasks, only set 'expanded' class if the link also has children within
    // the current menu.
    if ($data['link']['has_children'] && $data['below']) {
      $class[] = 'expanded';
    }
    elseif ($data['link']['has_children']) {
      $class[] = 'collapsed';
    }
    else {
      $class[] = 'leaf';
    }
    // Set a class if the link is in the active trail.
    if ($data['link']['in_active_trail']) {
      $class[] = 'active-trail active';
      $data['link']['localized_options']['attributes']['class'][] = 'active-trail active';
    }
    // Normally, l() compares the href of every link with $_GET['q'] and sets
    // the active class accordingly. But local tasks do not appear in menu
    // trees, so if the current path is a local task, and this link is its
    // tab root, then we have to set the class manually.
    if ($data['link']['href'] == $router_item['tab_root_href'] && $data['link']['href'] != $_GET['q']) {
      $data['link']['localized_options']['attributes']['class'][] = 'active';
    }

    // Allow menu-specific theme overrides.
    $element['#theme'] = 'menu_link__' . strtr($data['link']['menu_name'], '-', '_');
    $element['#attributes']['class'] = $class;
    $element['#title'] = $data['link']['title'];
    $element['#href'] = $data['link']['href'];
    $element['#localized_options'] = !empty($data['link']['localized_options']) ? $data['link']['localized_options'] : array();
    $element['#below'] = $data['below'] ? menu_tree_output($data['below']) : $data['below'];
    $element['#original_link'] = $data['link'];
    // Index using the link's unique mlid.
    $build[$data['link']['mlid']] = $element;
  }
  if ($build) {
    // Make sure drupal_render() does not re-order the links.
    $build['#sorted'] = TRUE;
    // Add the theme wrapper for outer markup.
    // Allow menu-specific theme overrides.
    $build['#theme_wrappers'][] = 'menu_tree__' . strtr($data['link']['menu_name'], '-', '_');
  }

  return $build;
}

/**
 * menu trees are rendered with <dl>
 */
function fast_foundation_menu_tree($variables) {
  return '<dl class="nice vertical tabs">' . $variables['tree'] . '</dl>';
}

/**
 * menu trees are rendered with <dl>
 */
function fast_foundation_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';
  
  if(in_array('active-trail', $element['#attributes']['class'])) {
  	$element['#attributes']['class'][] = 'active';
  }
  
  $element['#attributes']['class'][] = 'main';
  
  if ($element['#below']) {
  	// if we're 'below' we are a child and need an indicator
  	$element['#title'] = '(top) '.$element['#title'];
    $sub_menu = drupal_render($element['#below']);

  } 

  $element['#localized_options']['#html'] = true;
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<dd' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</dd>\n";
}

/**
 * calculate the width of the content block depending on whether there is a sidebars or not
 */
function fast_foundation_content_columns(&$page) {
	$columns = 'twelve';
	
	if ($page['sidebar_first']) {
		$columns = 'nine';
	}
	if ($page['sidebar_second']) {
		$columns = 'nine';
	}
	if ($page['sidebar_first'] && $page['sidebar_second']) {
		$columns = 'six';
	}
	
	return $columns;
}

/**
 * output the secondary menu with our own markup and style
 */
function fast_foundation_links__system_secondary_menu($variables) {
  $html = '<nav id="secondary">';
  $html .= "  <dl class='sub-nav'>\n";  
  foreach ($variables['links'] as $link) {
    $html .= "<dd>".l($link['title'], $link['href'], $link)."</dd>";
  }
  $html .= "  </dl>\n";
  $html .= "</nav>\n";

  return $html;
}

/**
 * output the main menu with our own markup and style including drop downs
 */
function fast_foundation_links__system_main_menu($variables) {
	
	// opening markup
	$html = '<nav id="main-menu">';
	$html .= '<ul class="nav-bar">';
	
	$main_menu = menu_build_tree( 'main-menu' );
	
	foreach ( $main_menu as $menu_item ) {
		// bring the link into the scope
		$link = $menu_item['link'];
		
/*
		print '<pre>';
		print_r($menu_item['link'] );
		print '</pre>';
*/
		
		// check if the menu item has any children and respond accordingly
		if ( $link['has_children'] == 0 ) {
				// no children
				$html .= '<li>';
				$html .= l($link['title'], $link['href'], array( 'attributes' => array( 'class' => array( 'main' ), ) ));
				$html .= '</li>';
		} else if ( $link['has_children'] >= 1  ) {
				// one or more children
				$html .= '<li class="has-flyout">';
				$html .= l($link['title'], $link['href'], array( 'attributes' => array( 'class' => array( 'main' ), ) ));
				$html .= '<a href="#" class="flyout-toggle"><span></span></a>';
				
				// get the children menu items from the [below] key
				
				//opening markup for children
				$html .= '<div class="flyout small">';
				$html .= '<ul>';
				
				foreach ( $menu_item['below'] as $child_menu_item ) {
					// bring the current menu link into scope
					$link = $child_menu_item['link'];
					$html .= '<li>';
					$html .= l($link['title'], $link['href'], array( 'attributes' => array( 'class' => array( 'main' ), ) ));
					$html .= '</li>';
				}
				// closing markup for children
				$html .= '</ul>';
				$html .= '</div>';
				$html .= '</li>';
		
		}
		
	}
	// closing markup
	$html .= '</ul>';
	$html .= '</nav>';
	
	return $html;
}

